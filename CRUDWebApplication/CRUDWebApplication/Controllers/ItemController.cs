﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRUDWebApplication.ViewModel;
using CRUDWebApplication.Models;

namespace CRUDWebApplication.Controllers
{
    public class ItemController : Controller
    {
        // GET: Item
        public ActionResult Index()
        {
            ItemClient client = new ItemClient();
            ViewBag.ItemCodeList = client.findAll();
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(ItemViewModel it)
        {
            ItemClient client = new ItemClient();
            client.Create(it.itemModel);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            ItemClient client = new ItemClient();
            client.Delete(id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ItemClient client = new ItemClient();
           ItemViewModel CVM = new ItemViewModel();
            CVM.itemModel = client.find(id);
            return View("Edit", CVM);
        }
        [HttpPost]
        public ActionResult Edit(ItemViewModel CVM)
        {
            ItemClient client = new ItemClient();
            client.Edit(CVM.itemModel);
            return RedirectToAction("Index");
        }
    }
}
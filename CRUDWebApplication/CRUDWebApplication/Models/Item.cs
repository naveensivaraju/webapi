﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CRUDWebApplication.Models
{
    public class Item
    {
        [Required]
        public string ITCODE { get; set; }

        [Required,MaxLength(15)]
        public string ITDESC { get; set; }

        public string ITRATE { get; set; }
    }
}